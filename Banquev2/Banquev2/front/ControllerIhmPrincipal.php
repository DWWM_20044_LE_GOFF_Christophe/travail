<?php
require_once "controllers/CreerAgenceController.php";
class ControllerIhmPrincipal
{
    private CreerAgenceController $creerAgenceController;

    public function __construct()
    {
        $this->creerAgenceController = new CreerAgenceController();
    }


    private function afficherMenuPrincipale(): void
    {
        echo "\n";
        echo " ----------------------------   MENU BANQUE DWWM 2004   ---------------------------\n\n";
        echo ("  1 - Créer une agence" . PHP_EOL);
        echo ("  2 - Créer un client" . PHP_EOL);
        echo ("  3 - Créer un compte bancaire" . PHP_EOL);
        echo ("  4 - Recherche de compte (numéro de compte)" . PHP_EOL);
        echo ("  5 - Recherche de client (Nom , Numéro de compte ou identifiant de client)" . PHP_EOL);
        echo ("  6 - Afficher la liste des comptes d’un client (identifiant client)" . PHP_EOL);
        echo ("  7 - Imprimer les infos client (identifiant client)" . PHP_EOL);
        echo ("  8 - Quitter le programme" . PHP_EOL);
        echo ("\n");
    }

    public function startApp()
    {
        while (true) {
            $this->afficherMenuPrincipale();
            
            $choix = readline(" Votre choix : ");
            $choix = strtoupper($choix);
            if ($choix != "1" && $choix != "2" && $choix != "3" && $choix != "4" && $choix != "5" && $choix != "6" && $choix != "7" && $choix != "8") {
                while ($choix != "1" && $choix != "2" && $choix != "3" && $choix != "4" && $choix != "5" && $choix != "6" && $choix != "7" && $choix != "8") {
                    $choix = strtoupper(readline(" Saisir un choix entre 1 et 11 : "));
                }
            }
            switch ($choix) {
                case "1":
                    $this->creerAgenceController->execute();
                    break;
                case "2":
                    //creationClient($clients, $donnees);
                    //print_r($clients);
                    break;
                case "3":
                    //creationCompte($clients, $compte_a_ouvrir, $comptes, $donnees1, $agences, $id_possible2, $donnees);
                    //print_r($comptes);
                    break;
                case "4":
                    //rechercheCompte($comptes);
                    break;
                case "5":
                    //recherche_client($clients, $comptes);
                    break;
                case "6":
                    //recherche_compte_client($clients, $comptes);
                    break;
                case "7":
                    //imprimer_info($clients, $comptes);
                    break;
                case "8":
                    echo ("\n                                 AU REVOIR   :)   \n");
                    echo ("\n");
                    $break = false;
                    break;
            }
        }
    }
}
