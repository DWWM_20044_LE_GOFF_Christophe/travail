<?php

require_once "ConstantesDao.php";

class CompteDao
{

    private const FILE_SAVE_COMPTE = "c:/envdev/donnees/save_comptes.csv";
    private const FILE_CPT_COMPTE = "c:/envdev/donnees/compteurs/cpt_comptes.txt";
    private const CHAMP_ID = "id";
    private const CHAMP_TYPE = "type";
    private const CHAMP_SOLDE = "solde";
    private const CHAMP_EST_AUTORISE = "estAutorise";
    private const CHAMP_CLIENT = "id_client";
    private const CHAMP_AGENCE = "id_agence";
    
    
    public function construct(){
        $this->agenceDao = new AgenceDao();
        $this->clientDao = new ClientDao(); 
    }


    private const ENTETES_COMPTES = [
        CompteDao::CHAMP_ID,
        CompteDao::CHAMP_TYPE,
        CompteDao::CHAMP_SOLDE,
        CompteDao::CHAMP_EST_AUTORISE,
        CompteDao::CHAMP_CLIENT,
        CompteDao::CHAMP_AGENCE
    ];
    public function saveAll(array $comptes): void
    {
        $handle = fopen(CompteDao::FILE_SAVE_COMPTE, ConstantesDao::FILE_OPTION_W_PLUS);
        if (!empty(CompteDao::ENTETES_COMPTES)) {
            fputcsv($handle, CompteDao::ENTETES_COMPTES, ConstantesDao::DELIM);
        }
        foreach ($comptes as $compte) {
            fputcsv($handle, $compte->toArray(), ConstantesDao::DELIM);
        }
        fclose($handle);
    }

    public function getByNumeroCompte($motif): Compte
    {
        return $this->getOneByAttribute(CompteDao::CHAMP_ID, $motif);
    }
    public function getByIdClient($motif): array
    {
        $allEntities = $this->getAll();
        $entitiesCherchees = [];
        foreach ($allEntities as $entity) {

            if ($entity->client!=null && strtolower($entity->client->getId()) === strtolower($motif)) {
                $entitiesCherchees[] = Compte::CompteFromArray($entity);
            }
        }
        return $entitiesCherchees;
    }

    public function getByIdAgence($motif): array
    {
        $allEntities = $this->getAll();
        $entitiesCherchees = [];
        foreach ($allEntities as $entity) {

            if ($entity->agence!=null && strtolower($entity->agence->getId()) === strtolower($motif)) {
                $compte = Compte::CompteFromArray($entity);

                $entitiesCherchees[] = $compte;
            }
        }
        return $entitiesCherchees;
    }

    public function getAll(): array
    {
        $handle = fopen(CompteDao::FILE_SAVE_COMPTE, ConstantesDao::FILE_OPTION_R);
        $entities = [];

        $entetes = fgetcsv($handle, 0, ConstantesDao::DELIM);

        while (($entity = fgetcsv($handle, 0, ConstantesDao::DELIM)) != false) {
            $compte =  Compte::CompteFromArray(array_combine($entetes, $entity));
            $agence = $this->agenceDao->getById($compte->getAgence()->getId());
            $compte->setAgence($agence);
            $client = $this->clientDao->getById($compte->getClient()->getId());
            $compte->setClient($client);
            $entities[] = $compte;
        }

        fclose($handle);
        return $entities;
    }
    public function deleteById(int $idEntity): void
    {
        $allEntities = $this->getAll();
        for ($i = 0; $i < count($allEntities); $i++) {
            if ($allEntities[$i]->CHAMP_ID === $idEntity) {
                array_splice($allEntities, $i, 1);
            }
        }
        $this->saveAll($allEntities);
    }
    public function modify(Compte $newEntity): void
    {
        $allEntities = $this->getAll();
        foreach ($allEntities as $currentEntity) {
            if ($currentEntity->CHAMP_ID === $newEntity[CompteDao::CHAMP_ID]) {
                $currentEntity = $newEntity;
            }
        }
        $this->saveAll($allEntities);
    }


    public function save(Compte $newCompte): Compte
    {
        $handle = fopen(CompteDao::FILE_SAVE_COMPTE, ConstantesDao::FILE_OPTION_A_PLUS);
        $newCompte->setId(str_pad($this->getNextId(), 3, "0", STR_PAD_LEFT));
        fputcsv($handle, $newCompte->toArray(), ConstantesDao::DELIM);
        fclose($handle);
        return $newCompte;
    }



    public function getNextId(): int
    {
        $handle = fopen(CompteDao::FILE_CPT_COMPTE, ConstantesDao::FILE_OPTION_A_PLUS);
        $currentId = intval(fgets($handle));
        $currentId++;
        fclose($handle);
        $handle = fopen(CompteDao::FILE_CPT_COMPTE, ConstantesDao::FILE_OPTION_W_PLUS);
        fputs($handle, $currentId);
        fclose($handle);
        return $currentId;
    }

    public function getOneByAttribute(string $attribute, string $motif): ?Compte
    {
        $allEntities = $this->getAll();
        foreach ($allEntities as $entity) {
            if (strtolower($entity->$attribute) === strtolower($motif)) {
                return $entity;
            }
        }
        return null;
    }
    public function getAllByAttribute(string $attribute, string $motif): array
    {
        $allEntities = $this->getAll();
        $entitiesCherchees = [];
        foreach ($allEntities as $entity) {
            if (strtolower($entity->$attribute) === strtolower($motif)) {
                $entitiesCherchees[] = Compte::CompteFromArray($entity);
            }
        }
        return $entitiesCherchees;
    }
}
